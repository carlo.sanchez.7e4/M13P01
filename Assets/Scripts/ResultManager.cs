﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ResultManager : MonoBehaviour
{
    public string[] respuestas;
    public string[] respuestasUsuario;
    public Text textResultado;

    // Start is called before the first frame update
    void Start()
    {
        textResultado = GameObject.Find("TextResultat").GetComponent<Text>();       
        respuestas = LaminaButton.respuestas;
        respuestasUsuario = LaminaButton.respuestasUsuario;
        textResultado.text = calculate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private string calculate() {
        int total = 12;
        int correctas = 0;

        for (int i = 0; i < total; i++) {
            if (respuestas[i].Equals(respuestasUsuario[i])) {
                correctas++;
            }
        }
        return string.Format("{0} / {1}", correctas, total);
    }
}
