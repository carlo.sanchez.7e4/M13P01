﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LaminaButton : MonoBehaviour
{
    [SerializeField]
    public int index;
    public static string[] respuestas;
    public static string[] respuestasUsuario;

    public Dropdown dropdown;
    public Dropdown dropdown2;
    public int menuIndex;
    public int menuIndex2;
    public List<Dropdown.OptionData> menuOptions;
    public List<Dropdown.OptionData> menuOptions2;
    public string value;
    public string value2;

    public bool dosXifres;

    // Start is called before the first frame update
    void Start()
    {
        index = 0;
        dropdown = GameObject.Find("Dropdown").GetComponent<Dropdown>();
        dropdown2 = GameObject.Find("Dropdown2").GetComponent<Dropdown>();
        menuOptions = dropdown.options;
        menuOptions2 = dropdown2.options;
        // Orden laminas (Provisional) -> N3, N5, N45, N74, N16, N2, N6, N97, N8, N7, N29, N42
        respuestas = new string[] { "3", "5", "45", "74", "16", "2", "6", "97", "8", "7", "29", "42" };
        respuestasUsuario = new string[12];
    }

    // Update is called once per frame
    void Update()
    {
        menuIndex = dropdown.value;
        menuIndex2 = dropdown2.value;
        value = menuOptions[menuIndex].text;
        value2 = menuOptions2[menuIndex2].text;

        dosXifres = respuestas[index].Length == 2;

        if (dosXifres)
        {
            dropdown2.gameObject.SetActive(true);
        } else
        {
            dropdown2.gameObject.SetActive(false);
        }
    }

    public void NextButton()
    {
        if (index < 11)
        {
            if (dosXifres)
            {
                respuestasUsuario[index] = value + value2;
                Debug.Log("Respuesta: " + respuestasUsuario[index]);
            }
            else
            {
                respuestasUsuario[index] = value;
                Debug.Log("Respuesta: " + respuestasUsuario[index]);
            }
            index++;
            Debug.Log("Index: " + index);
        } else if (index >= 11)
        {
            SceneManager.LoadScene("ResultScreen");
        }
        
    }

    public void BackButton()
    {
        if (index > 0)
        {
            index--;
            Debug.Log("Index: " + index);
        } 
    }
}
