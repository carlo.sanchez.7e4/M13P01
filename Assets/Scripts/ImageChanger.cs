﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageChanger : MonoBehaviour
{
    [SerializeField]
    public Image image;
    public Sprite[] imageArray;
    
    public LaminaButton LB;
    

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        LB = GameObject.Find("ControlLamina").GetComponent<LaminaButton>();
    }

    // Update is called once per frame
    void Update()
    {
        image.sprite = imageArray[LB.index];
    }
}
